function checkConnection() {
	
    var networkState = navigator.connection.type;
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    console.log("Available network connection: "+states[networkState]);
    if (states[networkState]=="No network connection"){
    	//alert("network missing");
        navigator.notification.alert(
        		'Please connect to the Internet and then launch the app again.',  // message
        	    exitApp,         					// callback
        	    'Internet connection required',     // title
        	    'Exit app'       					// buttonName
        	);   
        return false;
    }
    else
    	return true;
}

function checkAndroidVersion() {    
	var messageText = 'Hello, as you are using Android 4.4 at the moment you can browse the app. To join '+
		'and upload pictures go to www.mybristol.org';
    if (typeof (Storage) !== "undefined") {
	if (localStorage.hideWarning) {
	    //console.log("-----> Current agreement status is "+localStorage.agreement);
	} 
	else {
	    localStorage.hideWarning = 'no';
	}   
	console.log("-----> Current hideWarning status is "+localStorage.hideWarning);
    if (localStorage.hideWarning == 'no') {   
        var deviceOS  = device.platform  ;  //fetch the device operating system
        var deviceOSVersion = device.version ;  //fetch the device OS version
        console.log("Device OS: " + deviceOS); 
        console.log("Device OS Version: " + deviceOSVersion);
        var versionNum = deviceOSVersion.substr(0, 3);
        if (versionNum=='4.4'){
            navigator.notification.confirm(
            		messageText,  				// message
            	    onConfirmBug,         		// callback
            	    'Incompatibility warning',  // title
            	    ['Ignore this warning','Go to mybristol.org.uk']       // buttonName
            	); 
        } 
	}
    } else {
	console.log("Sorry, your device does not support local storage...");
    }
}

function exitApp() {
	navigator.app.exitApp();
}

function showTC() {    
    if (typeof (Storage) !== "undefined") {
	if (localStorage.agreement) {
	    //console.log("-----> Current agreement status is "+localStorage.agreement);
	} 
	else {
	    localStorage.agreement = 'no';
	}   
	console.log("-----> Current agreement status is "+localStorage.agreement);
    if (localStorage.agreement == 'no') {   
        navigator.notification.confirm(
        		TC,  // message
        	    onConfirmTC,         					// callback
        	    'Terms & Conditions',		     // title
        	    ['Agree','I Disagree']					// buttonName
        	);   
	}
    } else {
	console.log("Sorry, your device does not support local storage...");
    }
}

function onConfirmTC(buttonIndex) {
    //alert('You selected button ' + buttonIndex);
	if (buttonIndex==2){
		console.log("User did not agree with T&C, now exiting...");
		var ref = window.open('http://www.mybristol.org.uk', '_system');
		exitApp();  // exit app if user doesn't agree with Terms & Conditions
	}
	else if (buttonIndex ==1){
		userAgreement = true;
		writeTC();
	}
	else {
		exitApp();
	}
}

function onConfirmBug(buttonIndex) {
	if (buttonIndex==3){
		console.log("User chose to exit app due to incompatibility....");
		
		exitApp();
	}
	else if (buttonIndex ==1){
		// No need to do anything
		
	}
	else if (buttonIndex ==2){
		// Make sure that incompatibility warning does not appear again
		//writeWarning();
		var ref = window.open('http://www.mybristol.org.uk', '_system');
		setTimeout(function(){exitApp()},5000);
	}
	else {
		exitApp();
	}
}

function writeTC(){
	localStorage.agreement = 'yes';
}

function writeWarning(){
	localStorage.hideWarning = 'yes';
}